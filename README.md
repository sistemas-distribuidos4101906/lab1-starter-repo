# Control Version Lab 1: Basic Usage

Author: Vazquez Sanchez Erick Alejandro

This is a guide that includes all the steps I followed to complete the first lab. 

## Activities:

1. **Install git in your system and verify the git version installed.** 
    
    Step 1 - Install git.
    
    In my case I’m using an Ubuntu distribution, so the package manager that use to install git was apt. 
    
    ```
    sudo apt-get install git
    ```
    
    Step 2 - Verify git version. 
    
    To check the git version, use the command
    
    ```
    git --version
    ```
    
    Evidence
    
    ![Untitled](Control%20Version%20Lab%201%20Basic%20Usage%20bf47c796786e4aba91e3c283e9b89fc7/Untitled.png)
    
2. **Create a new account at https://gitlab.com**
    
    To create a new account on GitLab first you need to go into the website and click in the `Sign in`  button.
    
    ![Untitled](Control%20Version%20Lab%201%20Basic%20Usage%20bf47c796786e4aba91e3c283e9b89fc7/Untitled%201.png)
    
    After that you can sign in with some accounts as google, GitHub, etc. But for our purpose, lets click in `Register now`  anchor. 
    
    ![Untitled](Control%20Version%20Lab%201%20Basic%20Usage%20bf47c796786e4aba91e3c283e9b89fc7/Untitled%202.png)
    
    Fill the form with your information and click in `Register` button.
    
    ![Untitled](Control%20Version%20Lab%201%20Basic%20Usage%20bf47c796786e4aba91e3c283e9b89fc7/Untitled%203.png)
    
3. **Configure at least your username and email on the cli. Verify your information.**
    
    Step 1 - Configure name. 
    
    ```
    git config --global user.name "Erick Vazquez"
    ```
    
    Step 2 - Configure email.
    
    ```
    git config --global user.email "erickescorpions@comunidad.unam.mx"
    ```
    
    Step 3 - List Config Options.
    
    ```
    git config --list
    ```
    
    Evidence.
    
    ![Untitled](Control%20Version%20Lab%201%20Basic%20Usage%20bf47c796786e4aba91e3c283e9b89fc7/Untitled%204.png)
    
4. Create a new repository at [https://gitlab.com](https://gitlab.com) called lab1-starter-repo.
    
    Inside the Gitlab page, click in `New Project` .
    
    ![Untitled](Control%20Version%20Lab%201%20Basic%20Usage%20bf47c796786e4aba91e3c283e9b89fc7/Untitled%205.png)
    
    Select the option `Create blank project` and fill the fields to create the repository. 
    
    ![Untitled](Control%20Version%20Lab%201%20Basic%20Usage%20bf47c796786e4aba91e3c283e9b89fc7/Untitled%206.png)
    
    As a note, you can create a repository with a [README.md](http://readme.md/) file as default, or simply deselect the option to avoid that. Furthermore, you can configure the visibility of the repository; the 'private' option will make your repository secret to people who don’t have permissions, while 'public' will make your repository visible to all who have your repository URL.
    
    If you click on `Create project` , Gitlab will redirect you inside your new repository. 
    
    ![Untitled](Control%20Version%20Lab%201%20Basic%20Usage%20bf47c796786e4aba91e3c283e9b89fc7/Untitled%207.png)
    
5. Clone the repository in your environment, initialize, and synchronize with the remote repository. Use the following data to perform this task:
    1. Main branch name: main.
    2. file name : File1.txt
    3. Commit message: feat: Initial commit. 
    
    Step 1 - Clone the repository. 
    
    Once you determinate where you want to store the repository on your system file, you should clone the repository with the next command.  
    
    ```
    git clone https://gitlab.com/sistemas-distribuidos4101906/lab1-starter-repo.git
    ```
    
    That should generate a folder with the name of the project and the whole files in the remote repository should be inside that folder. 
    
    ![Untitled](Control%20Version%20Lab%201%20Basic%20Usage%20bf47c796786e4aba91e3c283e9b89fc7/Untitled%208.png)
    
    Step 2 - Create the main branch. 
    
    Once inside the project folder, we going to use the next command to create the branch.
    
    ```
    git switch --create main
    ```
    
    Step 3 - Create the file `File1.txt` .
    
    To create a new file, we going to use the next command.
    
    ```
    touch File1.txt
    ```
    
    Step 4 - Add the file into the stage. 
    
    ```
    git add .
    ```
    
    Step 5 - Commit the changes.
    
    ```
    git commit -m "feat: Initial commit"
    ```
    
    Evidence
    
    ![Untitled](Control%20Version%20Lab%201%20Basic%20Usage%20bf47c796786e4aba91e3c283e9b89fc7/Untitled%209.png)
    
6. Verify your commit history and create the graph
    
    To show the commit history we use the next command.
    
    ```
    git log
    ```
    
    To show a beautiful print of that we can use some flags, in this case we going to use graph and oneline flags.
    
    ```
    git log --graph --oneline
    ```
    
    Evidence
    
    ![Untitled](Control%20Version%20Lab%201%20Basic%20Usage%20bf47c796786e4aba91e3c283e9b89fc7/Untitled%2010.png)
    
7. Create a new file called File2.txt, up to the preparation area, and revert the change.
    
    Step 1 - Create the file and adding into the stage. 
    
    ```
    touch File2.txt
    git add .
    ```
    
    Step 2 - Check the status of the current changes. 
    
    ```
    git status
    ```
    
    ![Untitled](Control%20Version%20Lab%201%20Basic%20Usage%20bf47c796786e4aba91e3c283e9b89fc7/Untitled%2011.png)
    
    Step 3 - Removing file from staging area. 
    
    After checking the status of the file, let’s removing from the staging area. 
    
    ```
    git reset HEAD File2.txt
    ```
    
    Evidence
    
    ![Untitled](Control%20Version%20Lab%201%20Basic%20Usage%20bf47c796786e4aba91e3c283e9b89fc7/Untitled%2012.png)
    
8. Use the file created in the previous step to make a new commit, and then revert that commit.
    
    Step 1 - Create the commit. 
    
    ```
    git add .
    git commit -m "feat: adding File2.txt"
    ```
    
    Step 2 - Revert the commit.
    
    First we get the hash of the commit with git log. 
    
    ![Untitled](Control%20Version%20Lab%201%20Basic%20Usage%20bf47c796786e4aba91e3c283e9b89fc7/Untitled%2013.png)
    
    After getting the hash, let’s remove the commit with the next code.
    
    ```
    git revert 94d19ce
    ```
    
    Evidence
    
    ![Untitled](Control%20Version%20Lab%201%20Basic%20Usage%20bf47c796786e4aba91e3c283e9b89fc7/Untitled%2014.png)
    
9. Create a new branch from main called feature/create-login.
    
    ```
    git checkout -b feature/create-login
    ```
    
10. List all the branches. 
    
    ```
    git branch -a
    ```
    
    Evidence
    
    ![Untitled](Control%20Version%20Lab%201%20Basic%20Usage%20bf47c796786e4aba91e3c283e9b89fc7/Untitled%2015.png)
    
11. Change to the main branch and delete the branch created previously. 
    
    Step 1 - Changing branches. 
    
    ```
    git checkout main
    ```
    
    Step 2 - Removing branch creating below. 
    
    ```
    git branch -d feature/create-login
    ```
    
    Evidence
    
    ![Untitled](Control%20Version%20Lab%201%20Basic%20Usage%20bf47c796786e4aba91e3c283e9b89fc7/Untitled%2016.png)
    
12. Create a new branch from main named feature/wrong-name and then rename it to feature/good-one.
    
    Step 1 - Creating the branch and returning to main branch. 
    
    ```
    git checkout -b feature/wrong-name && git checkout main
    ```
    
    Step 2 - Renaming the branch. 
    
    ```
    git branch -m feature/wrong-name feature/good-one
    ```
    
    Evidence
    
    ![Untitled](Control%20Version%20Lab%201%20Basic%20Usage%20bf47c796786e4aba91e3c283e9b89fc7/Untitled%2017.png)
    
13. Utilize the renamed branch to make at least five commits. Afterward, review the commit history of that branch. 
    
    Step 1 - Changing branches. 
    
    ```
    git checkout feature/good-one
    ```
    
    Step 2 - Generating 5 commits. 
    
    To this I decide to add some files, as we can see in the next image. 
    
    ![Untitled](Control%20Version%20Lab%201%20Basic%20Usage%20bf47c796786e4aba91e3c283e9b89fc7/Untitled%2018.png)
    
    After that I commit every single file.
    
    Step 3 - Showing the commit history.
    
    ```
    git log --graph --oneline feature/good-one
    ```
    
    Evidence
    
    ![Untitled](Control%20Version%20Lab%201%20Basic%20Usage%20bf47c796786e4aba91e3c283e9b89fc7/Untitled%2019.png)
    
14. Using the WebUI, make a change in the feature/good-one. 
    
    Step 1 - Publishing branch in remote repository
    
    ```
    git push --set-upstream origin feature/good-one
    ```
    
    Step 2 - Making a change from web. 
    
    I decided to add a new file clicking in the plus icon. 
    
    ![Untitled](Control%20Version%20Lab%201%20Basic%20Usage%20bf47c796786e4aba91e3c283e9b89fc7/Untitled%2020.png)
    
    I fill the fields and click in `Commit changes` 
    
    ![Untitled](Control%20Version%20Lab%201%20Basic%20Usage%20bf47c796786e4aba91e3c283e9b89fc7/Untitled%2021.png)
    
15. In the CLI, check the status of our local repository.
    
    ![Untitled](Control%20Version%20Lab%201%20Basic%20Usage%20bf47c796786e4aba91e3c283e9b89fc7/Untitled%2022.png)
    
16. Retrieve the latest changes form the remote repository. 
    
    ![Untitled](Control%20Version%20Lab%201%20Basic%20Usage%20bf47c796786e4aba91e3c283e9b89fc7/Untitled%2023.png)
    
17. Sync both repositories. 
    
    ![Untitled](Control%20Version%20Lab%201%20Basic%20Usage%20bf47c796786e4aba91e3c283e9b89fc7/Untitled%2024.png)
    
18. Using the same branch, create a “.gitignore” file and add a minimum of five entries to it, then test its functionality. Sync your changes. 
    
    ![Untitled](Control%20Version%20Lab%201%20Basic%20Usage%20bf47c796786e4aba91e3c283e9b89fc7/Untitled%2025.png)
    
19. Finally, create a new merge request form our branch to the main, make the merge and clean your branches on both repositories (Local an remote). 
    
    Finally to made a merge request I go into the remote repository and go to the `feature/good-one` branch.
    
    After that in the side bar i go into Merge Request. 
    
    Once inside i click in `Create merge request` and fill all the fields required.
    
    ![Untitled](Control%20Version%20Lab%201%20Basic%20Usage%20bf47c796786e4aba91e3c283e9b89fc7/Untitled%2026.png)
    
    With the merge request published I accept the merge by click on `merge` 
    
    ![Untitled](Control%20Version%20Lab%201%20Basic%20Usage%20bf47c796786e4aba91e3c283e9b89fc7/Untitled%2027.png)
    
    ![Untitled](Control%20Version%20Lab%201%20Basic%20Usage%20bf47c796786e4aba91e3c283e9b89fc7/Untitled%2028.png)
    
    To update the local repository i made a pull from main branch
    
    ```
    git pull origin main 
    ```
    
    And delete the branch `features/good-one` 
    
    ```
    git branch -d features/good-one
    ```
    
    I do not know why `features/good-one` did not delete from remote repository, but to fix that I delete it from terminal. 
    
    ```
    git push origin --delete features/good-one
    ```
    
    Evidence
    
    ![Untitled](Control%20Version%20Lab%201%20Basic%20Usage%20bf47c796786e4aba91e3c283e9b89fc7/Untitled%2029.png)
    
    ![Untitled](Control%20Version%20Lab%201%20Basic%20Usage%20bf47c796786e4aba91e3c283e9b89fc7/Untitled%2030.png)
    
    ![Untitled](Control%20Version%20Lab%201%20Basic%20Usage%20bf47c796786e4aba91e3c283e9b89fc7/Untitled%2031.png)
    
    ![Untitled](Control%20Version%20Lab%201%20Basic%20Usage%20bf47c796786e4aba91e3c283e9b89fc7/Untitled%2032.png)